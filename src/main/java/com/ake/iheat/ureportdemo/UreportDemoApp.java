package com.ake.iheat.ureportdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UreportDemoApp 
{
    public static void main( String[] args )
    {
    	SpringApplication.run(UreportDemoApp.class, args);
    }
}
