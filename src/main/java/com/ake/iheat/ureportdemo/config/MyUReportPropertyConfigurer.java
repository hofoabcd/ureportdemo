package com.ake.iheat.ureportdemo.config;

import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.core.io.ClassPathResource;

import com.bstek.ureport.UReportPropertyPlaceholderConfigurer;

/**
 * 继承UReportPropertyPlaceholderConfigurer, 装载application.yml
 * @author hofoabcd
 */
public class MyUReportPropertyConfigurer extends UReportPropertyPlaceholderConfigurer {

	public MyUReportPropertyConfigurer(String path) {
		YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
		yaml.setResources(new ClassPathResource(path));
		this.setProperties(yaml.getObject());
	}
	
}
