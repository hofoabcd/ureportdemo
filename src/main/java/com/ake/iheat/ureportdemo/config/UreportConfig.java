package com.ake.iheat.ureportdemo.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.env.Environment;

import com.bstek.ureport.console.UReportServlet;

/**
 * UReport 配置
 * @author hofoabcd
 */
@Configuration
@ImportResource("classpath:ureport-console-context.xml")
public class UreportConfig implements EnvironmentAware {
	
	private Environment environment;
	
	@Override
	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}
	
	@Bean
	public MyUReportPropertyConfigurer propertyrConfigurer(){
		return new MyUReportPropertyConfigurer("application.yml");
	}
	
    @Bean
    public ServletRegistrationBean<UReportServlet> buildUreportServlet(){
        return new ServletRegistrationBean<UReportServlet>(new UReportServlet(), "/ureport/*");
    }

}
