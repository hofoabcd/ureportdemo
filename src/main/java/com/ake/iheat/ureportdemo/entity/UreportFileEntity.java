package com.ake.iheat.ureportdemo.entity;

import java.util.Date;
import lombok.Data;
 
@Data
public class UreportFileEntity {
	
	private Long id;
	private String name;
	private byte[] content;
	private Date createTime;
	private Date updateTime;
	
}
