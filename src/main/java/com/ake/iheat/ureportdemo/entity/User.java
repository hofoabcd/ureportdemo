package com.ake.iheat.ureportdemo.entity;

import lombok.Data;

@Data
public class User {

    private int id;
    private String name;
    private int salary;

}
